package com.project.steps;

import com.capgemini.ourWebdriver.BrowserFactory;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class loan_steps {
    private WebDriver browser = BrowserFactory.getWebDriver();

    @Given("^I have opened the loan request page$")
    public void iHaveOpenedTheLoanRequestPage(){
        browser.get(System.getProperty("user.dir") +"\\src\\test\\java\\com\\project\\resources\\website\\index.html");
    }


    @And("^I select loan type 'Car-loan'$")
    public void iSelectLoanTypeCarLoan(){
        browser.findElement(By.cssSelector("input[value='Car-loan']")).click();
    }

    @And("^the amount I want to borrow is '1000'$")
    public void theAmountIWantToBorrowIs() throws Throwable {
        browser.findElement(By.cssSelector("input[name='amount']")).sendKeys("1000");
        Thread.sleep(5000);
    }
}
